<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DB;
use PDF;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {


        $data = DB::table('cms_users')->get();
        // dd($data);
        return view('welcome', compact('data'));
    }

    public function inde()
    {
        $data = [
            'title' => 'First PDF for Medium',
            'heading' => 'Hello from 99Points.info',
            'content' => 'কদফঝকজ',
        ];

        $pdf = PDF::loadView('welcome', $data);
        return $pdf->download('medium.pdf');
    }
}
